package iostreams;

public class Root2 {
	public static void main(String[] args) {
		int i = 2;
		double r = Math.sqrt(i);
		System.out.format("The square root of %d is %.3f. or  %2$.5f  %n", i, r);
	}
}
