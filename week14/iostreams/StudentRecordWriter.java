package iostreams;

import java.util.*;
import java.text.*;
import java.io.*;
 
/**
 * StudentRecordWriter.java
 * This program illustrates how to use the ObjectOutputStream class for writing
 * a list of objects to a file.
 *
 * @author www.codejava.net
 */
public class StudentRecordWriter {
 
    public static void main(String[] args) {
        
 
        String outputFile = "week14/students.rec";
 
        DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
 
        try (
            ObjectOutputStream objectOutput
                = new ObjectOutputStream(new FileOutputStream(outputFile));
            ) {
 
            List<Student> listStudent = new ArrayList<>();
 
            listStudent.add(
                new Student("Alice", dateFormat.parse("02-15-1993"), false, 23, 80.5f));
 
            listStudent.add(
                new Student("Brian", dateFormat.parse("10-03-1994"), true, 22, 95.0f));
 
            listStudent.add(
                new Student("Carol", dateFormat.parse("08-22-1995"), false, 21, 79.8f));
 
            for (Student student : listStudent) {
                objectOutput.writeObject(student);
            }
 
        } catch (IOException | ParseException ex) {
            ex.printStackTrace();
        }
    }
}